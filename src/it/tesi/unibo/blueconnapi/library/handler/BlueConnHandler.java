package it.tesi.unibo.blueconnapi.library.handler;

import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConnHandler;
import android.os.Handler;
import android.os.Message;

public abstract class BlueConnHandler extends Handler implements IBlueConnHandler{

	public abstract void handleMessage(Message msg);
	
}
