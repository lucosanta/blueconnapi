package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.interfaces.IExecutorConnection;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.os.Handler;


public class Runnables implements IExecutorConnection{
	final static boolean D = true;
	private Handler handler;
	private ThreadPoolExecutor executor;
	public static List<Manage> manageThreads = new ArrayList<Manage>();
	private Accept acceptThread;
	private Multi connectMThread;
	private Connect connectSThread;
	public static boolean connectThreadIsRunning;
	public static Runnables run;
	public BlueConn b;

	static ArrayList<BluetoothDevice> usedDevices = new ArrayList<BluetoothDevice>();


	public Runnables(BlueConn b,Handler h) {
		run=this;
		handler = h;
		this.b=b;
		//mainActivity = m;
		executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	}

	@Override
	public void executeAccept(BluetoothAdapter a, int value) {
		acceptThread = new Accept(a,value,handler,executor);
		executor.execute(acceptThread);
		//executor.execute(new Accept(a, value));
	}

	@Override
	public void executeConnect(List<BluetoothDevice> d,BluetoothAdapter a, int value) {
		for(int i=0;i<d.size();i++){
			if(d.get(i)==null){
				d.remove(i);
			}
		}
		connectMThread = new Multi(d,a,handler,executor);
		executor.execute(connectMThread);
	}

	@Override
	public void executeConnect(BluetoothDevice d,BluetoothAdapter a) {
		//executor.execute(new Connect(d, value));
		if(manageThreads.size()>=7){
			manageThreads.get(0).cancel();
		}
		connectSThread = new Connect(d,a,0,handler,executor);
		executor.execute(connectSThread);
	}
	
	public void executeConnect2All(BluetoothDevice d, BluetoothAdapter a, String text){
		if(manageThreads.size()>=7){
			manageThreads.get(0).cancel();
		}
		connectSThread = new Connect(d,a,0,handler,executor,text);
		executor.execute(connectSThread);
	}
	
	public void cancel() {
		usedDevices.clear();
		/*usedUUIDs.clear();*/
	}

	public static IExecutorConnection getInstance(){
		return run;
	}

	@Override
	public void cancelManage(int position) {
		manageThreads.get(position).cancel();
	}
}



/*

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import java.util.UUID;


public class Accept implements Runnable {
private BluetoothAdapter adapter;
private BluetoothSocket socket;
private int iterator;

public Accept(BluetoothAdapter adapter, int value) {
	this.adapter = adapter;
	iterator = value;

}

@Override
public void run() {
	try {
		handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "ACCEPTING THREAD RUN").sendToTarget();
		
		if(D) Log.d("DEBUG", "Listening with uuid: " + BlueConn.uuids[iterator].toString());
		BluetoothServerSocket serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(Integer.toString(iterator), BlueConn.uuids[iterator]);
		if(D) Log.d("DEBUG", "Accepting...");
		socket = serverSocket.accept();
		if(D) Log.d("DEBUG", "Connected. Closing server socket.");
		serverSocket.close();

		//TODO: (Accept) Pass the socket to another thread for read/write management.
		Manage thread = new Manage(socket,adapter);
		manageThreads.add(thread);
		executor.execute(thread);

		Log.d("DEBUG", "Not doing anything. Closing connected socket.");
		socket.close();
	}
	catch (IOException e) {
		if(D) Log.d("DEBUG", "Exception", e);
	}
}

public void cancel() {
	if (socket != null) {
		try {
			if(D) Log.d("DEBUG", "Cancelled connection. Socket is closing.");
			socket.close();
			socket = null;
		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Exception", e);
		}
	}
}
}*/

//---------------------------------------------------------------------

/*public class NewAccept implements Runnable {
private BluetoothAdapter adapter;
private BluetoothSocket socket;
private BluetoothServerSocket serverSocket;
public String DEVICE_NAME_ACCEPT="main";

public NewAccept(BluetoothAdapter a) {
	adapter = a;
}

@Override
public void run() {
	for (int i = 0; i < BlueConn.uuids.length; i++) {
		UUID uuid = BlueConn.uuids[i];
		if (usedUUIDs.size() > 0) {
			if (usedUUIDs.contains(uuid))
				continue;
		}
		try {
			if(D) Log.d("DEBUG", "Listening with uuid: " + uuid.toString());
			handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "NEWACCEPTING THREAD RUN").sendToTarget();
			serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord("test", uuid);
			socket = serverSocket.accept();
			if(D) Log.d("DEBUG", "Connected successfully.");
			serverSocket.close();
			Manage thread = new Manage(socket,adapter);
			manageThreads.add(thread);
			executor.execute(thread);
			usedUUIDs.add(uuid);
			break;
		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Unable to connect.", e);
		}
	}
}

public void cancel() {
	if (serverSocket != null) {
		try {
			serverSocket.close();
			serverSocket = null;
		}
		catch (IOException e1) {
			if(D) Log.d("DEBUG", "Unable to close server socket.", e1);
		}
	}
	if (socket != null) {
		try {
			socket.close();
			socket = null;
		}
		catch (IOException e1) {
			if(D) Log.d("DEBUG", "Unable to close socket.", e1);
		}
	}
}
}*/

//---------------------------------------------------------------------

/*public class Connect implements Runnable {
private BluetoothDevice device;
private BluetoothSocket socket;
private BluetoothAdapter adapter;
//private int iterator;
public Object monitor = new Object();

public Connect(BluetoothDevice d, BluetoothAdapter a, int value) {
	device = d;
	//iterator = value;
	adapter =a ;
}

@Override
public void run() {
	synchronized (monitor) {
		connectThreadIsRunning = true;
	}
	boolean success= false;
	for(int i=0;i<BlueConn.uuids.length;i++){
		if(!success){
			try{
				handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "CONNECTING THREAD RUN").sendToTarget();
				if(D) Log.d("DEBUG", "Device is creating socket with uuid: " + BlueConn.uuids[i].toString());
				socket = device.createInsecureRfcommSocketToServiceRecord(BlueConn.uuids[i]);
				if(D) Log.d("DEBUG", "Trying to connect...");
				socket.connect();
				if(D) Log.d("DEBUG", "Connection successful.");
				success = true;

			} catch(IOException e){
				success= false;

			}

		} else {
			break;
		}
	}
	//TODO: (Connect) Pass the socket to another thread for read/write management.
	Manage thread = new Manage(socket,adapter);
	manageThreads.add(thread);
	executor.execute(thread);
	Log.d("DEBUG", "Not doing anything, closing connected socket.");
	socket.close();
	synchronized (monitor) {
		connectThreadIsRunning = false;
	}
}

public void cancel() {
	if (socket != null) {
		try {
			if(D) Log.d("DEBUG", "Cancelled connection. Socket is closing.");
			socket.close();
			socket = null;
		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Exception", e);
		}
	}
}
}*/

//---------------------------------------------------------------------
//////////
/*boolean turnedon=false;
Vibrator vibrator;
long[] pattern= {0,200,100,300,400};
Camera camera;*/

//////////
/*BluetoothDevice blueRemoteDevManage,blueRemoteToCancel;

public class Manage implements Runnable {
private InputStream inputStream;
private OutputStream outputStream;
private BluetoothSocket socket;
private BluetoothAdapter adapterB;
String DEVICE_NAME,DEVICE_ADDRESS,DEVICE_NAMEADDR;
Context context;
boolean killed;

public Manage(BluetoothSocket s,BluetoothAdapter a) {
	socket = s;
	try {
		inputStream = s.getInputStream();
		outputStream = s.getOutputStream();
	}
	catch (IOException e) {
		if(D) Log.d("DEBUG", "Exception", e);
	}

	blueRemoteDevManage=s.getRemoteDevice();
	handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTED).sendToTarget();
	
	adapterB=a;
	DEVICE_NAME=adapterB.getName();
	DEVICE_ADDRESS=adapterB.getAddress();
	DEVICE_NAMEADDR = DEVICE_NAME+"/"+DEVICE_ADDRESS;
}

public String getNameAddr(){
	return DEVICE_NAMEADDR;
}

@Override
public void run() {
	byte[] buffer = new byte[256];
	int bytes = 0;
	while (!killed) {
		try {
			bytes = inputStream.read(buffer);
			handler.obtainMessage(BlueConnHandler.MSG_READ, bytes, 0, buffer).sendToTarget();

		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Exception", e);
			manageThreads.remove(this);
			break;
		}

	}
	cancel();
}

public void forward(byte[] data, int n){
	// if the current thread has different manage thread there is the need to forward all incoming message :
	//	- to their client for the server
	//  - to a server if the client is connected to another server
	if (outputStream != null) {
		try {
			for(Manage thread: manageThreads){
				if(thread.equals(this)){

				} else {

					thread.outputStream.write(data,0,n);
					handler.obtainMessage(BlueConnHandler.MSG_FORWARD, n, 0, data).sendToTarget();
				}
			}

		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Exception", e);
		}
	}	
}

public void write(byte[] data, int numberOfSameThread,String destinatario,int mode) {
	if (outputStream != null) {
		try {
			Log.d("Runnables Manage Write", "MSG_WRITE ");
			if(numberOfSameThread == 0){
				outputStream.write(data);
				handler.obtainMessage(BlueConnHandler.MSG_WRITE, data.length, 0, data).sendToTarget();
			}
		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Exception", e);
		}
	}
}

public void cancel() {
	try {
		blueRemoteToCancel = this.getBluetoothDeviceConnected();
		kill();
		handler.obtainMessage(BlueConnHandler.MSG_GOODBYENEIGHBOR).sendToTarget();
		manageThreads.remove(this);
		
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
		     // only for jelly bean and newer versions
			socket.close();
		}
		
	}
	catch (IOException e) {
		if(D) Log.d("DEBUG", "Exception", e);
	}
}

public void kill(){
	
}

public BluetoothDevice getBluetoothDeviceConnected(){
	BluetoothDevice d= socket.getRemoteDevice();
	return d;
}
}*/

//---------------------------------------------------------------------

/*public class Multi implements Runnable {

private Object monitor = new Object();
private boolean isComplete;
private boolean hasConnected;
private List<BluetoothDevice> list;
private BluetoothSocket socket;
private BluetoothAdapter adapter;

public Multi(List<BluetoothDevice> d,BluetoothAdapter a) {
	list = d;
	hasConnected = false;
	adapter = a ;
}

@Override
public void run() {
	Thread block = new Thread(new Runnable() {
		@Override
		public void run() {
			for (BluetoothDevice device : list) {
				if (usedDevices.size() > 0) {
					if (usedDevices.contains(device))
						continue;
				}
				for (int i = 0; i < BlueConn.uuids.length; i++) {

					try {
						if(device != null){
							if(device.getName() != null){
								handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "MULTICONNECT THREAD RUN").sendToTarget();
								if(D) Log.d("DEBUG", "Trying to connect device " + device.getName() + " with uuid: " + BlueConn.uuids[i]);
								socket = device.createInsecureRfcommSocketToServiceRecord(BlueConn.uuids[i]);
								socket.connect();
								if(D) Log.d("DEBUG", "Connected successfully.");
								hasConnected = true;
								Manage thread = new Manage(socket,adapter,handler);
								manageThreads.add(thread);
								executor.execute(thread);
								usedDevices.add(device);
								//usedUUIDs.add(Main.uuids[i]);
								break;
							}
						}
					}
					catch (IOException e) {
						if(D) Log.d("DEBUG", "Exception: Unsuccessful attempt at connecting with uuid: " + BlueConn.uuids[i], e);
					}
				}
			}

			if (!hasConnected)
				if(D) Log.d("DEBUG", "No connections made. Unsuccessful.");

			synchronized (monitor) {
				isComplete = true;
				monitor.notifyAll();
			}
		}
	});
	block.start();

	synchronized (monitor) {
		isComplete = false;
		while (!isComplete) {
			try {
				monitor.wait();
				if (isComplete)
					break;
			}
			catch (InterruptedException e) {
				if(D) Log.d("DEBUG", "Interrupted within Multi, ignoring...", e);
			}
		}
	}
}

public boolean hasConnected() {
	return hasConnected;
}

public void cancel() {
	if (socket != null) {
		try {
			socket.close();
			
			socket = null;
		}
		catch (IOException e) {
			if(D) Log.d("DEBUG", "Unable to close socket.", e);
		}
	}
}
}*/
