package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IManage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.util.Log;




public class Manage implements IManage {
	private InputStream inputStream;
	private OutputStream outputStream;
	private BluetoothSocket socket;
	private BluetoothAdapter adapterB;
	String DEVICE_NAME,DEVICE_ADDRESS,DEVICE_NAMEADDR;
	Context context;
	boolean killed;
	BluetoothDevice blueRemoteDevManage,blueRemoteToCancel;
	Handler handler;
	String text = "No Mex";

	public Manage(BluetoothSocket s,BluetoothAdapter a, Handler handler) {
		socket = s;
		this.handler= handler;
		try {
			inputStream = s.getInputStream();
			outputStream = s.getOutputStream();
		}
		catch (IOException e) {
			if(Runnables.D) Log.d("DEBUG", "Exception", e);
		}

		
	
		adapterB=a;
		DEVICE_NAME=adapterB.getName();
		DEVICE_ADDRESS=adapterB.getAddress();
		DEVICE_NAMEADDR = DEVICE_NAME+"/"+DEVICE_ADDRESS;
	}
	
	public Manage(BluetoothSocket s,BluetoothAdapter a, Handler handler,String text) {
		socket = s;
		this.handler= handler;
		try {
			inputStream = s.getInputStream();
			outputStream = s.getOutputStream();
		}
		catch (IOException e) {
			if(Runnables.D) Log.d("DEBUG", "Exception", e);
		}
		adapterB=a;
		DEVICE_NAME=adapterB.getName();
		DEVICE_ADDRESS=adapterB.getAddress();
		DEVICE_NAMEADDR = DEVICE_NAME+"/"+DEVICE_ADDRESS;
		String destinatario = s.getRemoteDevice().getName()+"/"+s.getRemoteDevice().getAddress();
		write(text.getBytes(),Integer.MAX_VALUE,destinatario);
		
	}
	
	public void sendTarget(int position){
		blueRemoteDevManage=socket.getRemoteDevice();
		String con = blueRemoteDevManage.getName()+"/"+blueRemoteDevManage.getAddress();
		handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTED,position, 0, con).sendToTarget();
	}
	
	public String getNameAddr(){
		return DEVICE_NAMEADDR;
	}

	@Override
	public void run() {
		byte[] buffer = new byte[256];
		int bytes = 0;
		while (!killed) {
			try {
				bytes = inputStream.read(buffer);
				handler.obtainMessage(BlueConnHandler.MSG_READ, bytes, 0, buffer).sendToTarget();

			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Exception", e);
				Runnables.manageThreads.remove(this);
				break;
			}

		}
		cancel();
	}
/*
	public void forward(byte[] data, int n){
		// if the current thread has different manage thread there is the need to forward all incoming message :
		//	- to their client for the server
		//  - to a server if the client is connected to another server
		if (outputStream != null) {
			try {
				for(Manage thread: manageThreads){
					if(thread.equals(this)){

					} else {

						thread.outputStream.write(data,0,n);
						handler.obtainMessage(BlueConnHandler.MSG_FORWARD, n, 0, data).sendToTarget();
					}
				}

			}
			catch (IOException e) {
				if(D) Log.d("DEBUG", "Exception", e);
			}
		}	
	}
*/
	@Override
	public void write(byte[] data, int numberOfSameThread,String destinatario) {
		if (outputStream != null) {
			try {
				Log.d("Runnables Manage Write", "MSG_WRITE ");
				if(numberOfSameThread == 0){
					outputStream.write(data);
					handler.obtainMessage(BlueConnHandler.MSG_WRITE, data.length, 0, data).sendToTarget();
				} else if(numberOfSameThread == Integer.MAX_VALUE){
					outputStream.write(data);
					handler.obtainMessage(BlueConnHandler.MSG_WRITE, data.length, 0, data).sendToTarget();
				}
			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Exception", e);
			}
		}
	}

	@Override
	public void cancel() {
		try {
			blueRemoteToCancel = this.getBluetoothDeviceConnected();
			killed=true;
			String blueHand= blueRemoteToCancel.getName()+"/"+blueRemoteToCancel.getAddress();
			handler.obtainMessage(BlueConnHandler.MSG_GOODBYENEIGHBOR,blueHand).sendToTarget();
			Runnables.manageThreads.remove(this);
			
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
			     // only for jelly bean and newer versions
				socket.close();
			}
			
		}
		catch (IOException e) {
			if(Runnables.D) Log.d("DEBUG", "Exception", e);
		}
	}

	@Override
	public BluetoothDevice getBluetoothDeviceConnected(){
		BluetoothDevice d= socket.getRemoteDevice();
		return d;
	}
}