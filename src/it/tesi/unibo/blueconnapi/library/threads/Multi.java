package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IActivationComm;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class Multi implements IActivationComm {

	private Object monitor = new Object();
	private boolean isComplete;
	private boolean hasConnected;
	private List<BluetoothDevice> list;
	private BluetoothSocket socket;
	private BluetoothAdapter adapter;
	Handler handler;
	ThreadPoolExecutor executor;

	public Multi(List<BluetoothDevice> d,BluetoothAdapter a,Handler handler,ThreadPoolExecutor executor) {
		list = d;
		hasConnected = false;
		adapter = a ;
		this.handler = handler;
		this.executor=executor;
	}

	@Override
	public void run() {
		Thread block = new Thread(new Runnable() {
			@Override
			public void run() {
				for (BluetoothDevice device : list) {
					if (Runnables.usedDevices.size() > 0) {
						if (Runnables.usedDevices.contains(device))
							continue;
					}
					for (int i = 0; i < BlueConn.uuids.length; i++) {

						try {
							if(device != null){
								if(device.getName() != null){
									handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "MULTICONNECT THREAD RUN").sendToTarget();
									if(Runnables.D) Log.d("DEBUG", "Trying to connect device " + device.getName() + " with uuid: " + BlueConn.uuids[i]);
									socket = device.createInsecureRfcommSocketToServiceRecord(BlueConn.uuids[i]);
									socket.connect();
									if(Runnables.D) Log.d("DEBUG", "Connected successfully.");
									hasConnected = true;
									Manage thread = new Manage(socket,adapter,handler);
									Runnables.manageThreads.add(thread);
									thread.sendTarget(Runnables.manageThreads.size()-1);
									executor.execute(thread);
									Runnables.usedDevices.add(device);
									/*usedUUIDs.add(Main.uuids[i]);*/
									break;
								}
							}
						}
						catch (IOException e) {
							if(Runnables.D) Log.d("DEBUG", "Exception: Unsuccessful attempt at connecting with uuid: " + BlueConn.uuids[i], e);
						}
					}
				}

				if (!hasConnected)
					if(Runnables.D) Log.d("DEBUG", "No connections made. Unsuccessful.");

				synchronized (monitor) {
					isComplete = true;
					monitor.notifyAll();
				}
			}
		});
		block.start();

		synchronized (monitor) {
			isComplete = false;
			while (!isComplete) {
				try {
					monitor.wait();
					if (isComplete)
						break;
				}
				catch (InterruptedException e) {
					if(Runnables.D) Log.d("DEBUG", "Interrupted within Multi, ignoring...", e);
				}
			}
		}
	}

	@Override
	public void cancel() {
		if (socket != null) {
			try {
				socket.close();
				
				socket = null;
			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Unable to close socket.", e);
			}
		}
	}
}
