package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.interfaces.IActivationComm;
import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConnHandler;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class Accept implements IActivationComm {
	
	private BluetoothAdapter adapter;
	private BluetoothSocket socket;
	private int iterator;
	private Handler handler;
	private ThreadPoolExecutor executor;	

	public Accept(BluetoothAdapter adapter, int value, Handler handler,ThreadPoolExecutor executor) {
		this.adapter = adapter;
		iterator = value;
		this.handler= handler;
		this.executor = executor;

	}

	@Override
	public void run() {
		try {
			handler.obtainMessage(IBlueConnHandler.MSG_DEBUG, "ACCEPTING THREAD RUN").sendToTarget();
			
			if(Runnables.D) Log.d("DEBUG", "Listening with uuid: " + BlueConn.uuids[iterator].toString());
			BluetoothServerSocket serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord(Integer.toString(iterator), BlueConn.uuids[iterator]);
			if(Runnables.D) Log.d("DEBUG", "Accepting...");
			socket = serverSocket.accept();
			if(Runnables.D) Log.d("DEBUG", "Connected. Closing server socket.");
			serverSocket.close();

			//TODO: (Accept) Pass the socket to another thread for read/write management.
			Manage thread = new Manage(socket,adapter,handler);
			Runnables.manageThreads.add(thread);
			thread.sendTarget(Runnables.manageThreads.size()-1);
			executor.execute(thread);

			/*Log.d("DEBUG", "Not doing anything. Closing connected socket.");
			socket.close();*/
		}
		catch (IOException e) {
			if(Runnables.D) Log.d("DEBUG", "Exception", e);
		}
	}

	@Override
	public void cancel() {
		if (socket != null) {
			try {
				if(Runnables.D) Log.d("DEBUG", "Cancelled connection. Socket is closing.");
				socket.close();
				socket = null;
			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Exception", e);
			}
		}
	}
}