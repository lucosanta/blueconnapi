package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IActivationComm;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class NewAccept implements IActivationComm {
	private BluetoothAdapter adapter;
	private BluetoothSocket socket;
	private BluetoothServerSocket serverSocket;
	public String DEVICE_NAME_ACCEPT="main";
	Handler handler; 
	ThreadPoolExecutor executor;
	

	public NewAccept(BluetoothAdapter a, Handler handler, ThreadPoolExecutor executor) {
		adapter = a;
		this.handler= handler;
		this.executor= executor;
	}

	@Override
	public void run() {
		for (int i = 0; i < BlueConn.uuids.length; i++) {
			UUID uuid = BlueConn.uuids[i];
			/*if (usedUUIDs.size() > 0) {
				if (usedUUIDs.contains(uuid))
					continue;
			}*/
			try {
				if(Runnables.D) Log.d("DEBUG", "Listening with uuid: " + uuid.toString());
				handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "NEWACCEPTING THREAD RUN").sendToTarget();
				serverSocket = adapter.listenUsingInsecureRfcommWithServiceRecord("test", uuid);
				socket = serverSocket.accept();
				if(Runnables.D) Log.d("DEBUG", "Connected successfully.");
				serverSocket.close();
				Manage thread = new Manage(socket,adapter,handler);
				Runnables.manageThreads.add(thread);
				thread.sendTarget(Runnables.manageThreads.size()-1);
				executor.execute(thread);
				/*usedUUIDs.add(uuid);*/
				break;
			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Unable to connect.", e);
			}
		}
	}

	@Override
	public void cancel() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
				serverSocket = null;
			}
			catch (IOException e1) {
				if(Runnables.D) Log.d("DEBUG", "Unable to close server socket.", e1);
			}
		}
		if (socket != null) {
			try {
				socket.close();
				socket = null;
			}
			catch (IOException e1) {
				if(Runnables.D) Log.d("DEBUG", "Unable to close socket.", e1);
			}
		}
	}
}