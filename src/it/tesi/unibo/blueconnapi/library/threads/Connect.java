package it.tesi.unibo.blueconnapi.library.threads;

import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IActivationComm;
import it.tesi.unibo.blueconnapi.library.system.BlueConn;

import java.io.IOException;
import java.util.concurrent.ThreadPoolExecutor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

public class Connect implements IActivationComm {
	private BluetoothDevice device;
	private BluetoothSocket socket;
	private BluetoothAdapter adapter;
	//private int iterator;
	public Object monitor = new Object();

	Handler handler;
	ThreadPoolExecutor executor;

	String text = "No Mex";

	
	public Connect(BluetoothDevice d, BluetoothAdapter a, int value, Handler handler , ThreadPoolExecutor executor) {
		device = d;
		//iterator = value;
		adapter =a ;

		this.handler= handler;
		this.executor=executor;

	}

	public Connect(BluetoothDevice d, BluetoothAdapter a, int value, Handler handler , ThreadPoolExecutor executor,String text) {
		device = d;
		//iterator = value;
		adapter =a ;
		this.text = text;
		this.handler= handler;
		this.executor=executor;

	}

	@Override
	public void run() {
		synchronized (monitor) {
			Runnables.connectThreadIsRunning = true;
		}
		boolean success= false;
		for(int i=0;i<BlueConn.uuids.length;i++){
			if(!success){
				try{
					handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "CONNECTING THREAD RUN").sendToTarget();
					if(Runnables.D) Log.d("DEBUG", "Device is creating socket with uuid: " + BlueConn.uuids[i].toString());
					socket = device.createInsecureRfcommSocketToServiceRecord(BlueConn.uuids[i]);
					if(Runnables.D) Log.d("DEBUG", "Trying to connect...");
					socket.connect();
					if(Runnables.D) Log.d("DEBUG", "Connection successful.");
					success = true;

				} catch(IOException e){
					success= false;

				}

			} else {
				break;
			}
		}
		Manage thread;
		if(text.equals("No Mex")){
			thread= new Manage(socket,adapter,handler);
		} else {	
			thread = new Manage(socket,adapter,handler,text);
		}
		Runnables.manageThreads.add(thread);
		thread.sendTarget(Runnables.manageThreads.size()-1);
		executor.execute(thread);
		/*Log.d("DEBUG", "Not doing anything, closing connected socket.");
		socket.close();*/
		synchronized (monitor) {
			Runnables.connectThreadIsRunning = false;
		}

	}

	@Override
	public void cancel() {
		if (socket != null) {
			try {
				if(Runnables.D) Log.d("DEBUG", "Cancelled connection. Socket is closing.");
				socket.close();
				socket = null;
			}
			catch (IOException e) {
				if(Runnables.D) Log.d("DEBUG", "Exception", e);
			}
		}
	}
}