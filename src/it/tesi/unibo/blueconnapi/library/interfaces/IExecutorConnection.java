package it.tesi.unibo.blueconnapi.library.interfaces;

import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

public interface IExecutorConnection {

	public void executeAccept(BluetoothAdapter a, int value);
	public void executeConnect(BluetoothDevice d,BluetoothAdapter a);
	public void executeConnect(List<BluetoothDevice> d,BluetoothAdapter a, int value);
	public void cancelManage(int position);
	
}
