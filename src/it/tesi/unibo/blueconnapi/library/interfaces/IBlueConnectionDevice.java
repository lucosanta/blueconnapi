package it.tesi.unibo.blueconnapi.library.interfaces;

import android.bluetooth.BluetoothDevice;

public interface IBlueConnectionDevice {

	public final static String MODE_ACTIVE="Active";
	public final static String MODE_INACTIVE="Inactive";
	public final static String MODE_CONNECTED="Connected";
	public final static String MODE_CONNECTING="Connecting...";
	public final static String MODE_DISCONNECTED="Disconnected";
	
	public BluetoothDevice getDevice();
	public String getRssi();
	public String getMode();
	
	public void setMode(String mode);
	public void setRssi(String rssi);
	public void setDevice(BluetoothDevice device);
	

}
