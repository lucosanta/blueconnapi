package it.tesi.unibo.blueconnapi.library.interfaces;

public interface IActivationComm extends Runnable {

	public void cancel();
	
	
}
