package it.tesi.unibo.blueconnapi.library.interfaces;

import android.bluetooth.BluetoothDevice;

public interface IManage extends IActivationComm{
	
	public void write(byte[] data, int numberOfSameThread,String destinatario);
	public BluetoothDevice getBluetoothDeviceConnected();
	
}
