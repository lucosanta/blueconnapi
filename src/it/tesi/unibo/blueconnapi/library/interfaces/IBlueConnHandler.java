package it.tesi.unibo.blueconnapi.library.interfaces;

public interface IBlueConnHandler{

	// Obtained if BlueConn find an error
		public static final int MSG_ERROR = 0x4000;
		// Obtained only if timeRefresh is gone to update the neighbor
		public static final int MSG_REFRESH = 0x4001;
		//
		public static final int MSG_READ = 0x4003;
		//
		public static final int MSG_WRITE = 0x4004;
		//
		public static final int MSG_NEWCONNECTION = 0x4005;
		//
		public static final int MSG_ACCEPTINGTHREAD = 0x4006;
		//
		public static final int MSG_CONNECTINGTHREAD = 0x4007;
		//
		public static final int MSG_NEWNEIGHBOR = 0x4008;
		//
		public static final int MSG_ONDISCOVERY = 0x4009;
		//
		public static final int MSG_DISCOVERYENDED = 0x4010;
		//
		public static final int MSG_RSSIREFRESH = 0x4011;
		//
		public static final int MSG_GOODBYENEIGHBOR = 0x4012;
		//
		public static final int MSG_STATECONNECTING = 0x4013;
		//
		public static final int MSG_STATECONNECTED = 0x4014;
		//
		public static final int MSG_DEBUG = 0x4fff;
		
}
