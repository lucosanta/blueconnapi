package it.tesi.unibo.blueconnapi.library.interfaces;

import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import java.util.List;


public interface IBlueConn {

	public static final String TAG= "BlueConn";
	public final static boolean D=true;
	static final int REQUEST_CONNECT_DEVICE=0x1001;
	static final int REQUEST_ENABLE_BLUETOOTH=0x1002;

	public static final int STARTMODE_P2P=0x3000;
	public static final int STARTMODE_CLIENT=0x3001;
	public static final int STARTMODE_SERVER=0x3002;
	
	public static final int WRITE_SINGLEPEER=0x5000;
	public static final int WRITE_BROADCAST=0x5001;
	
	public void initBluetooth();
	public void start(int mode);
	public List<BlueConnectionDevice> whoAreMyNeighbor();
	public void write(String text,String destinatario,int mode);
}
