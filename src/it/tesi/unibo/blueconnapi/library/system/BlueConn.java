package it.tesi.unibo.blueconnapi.library.system;

import it.tesi.unibo.blueconnapi.library.device.BlueConnectionDevice;
import it.tesi.unibo.blueconnapi.library.handler.BlueConnHandler;
import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConn;
import it.tesi.unibo.blueconnapi.library.threads.Runnables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class BlueConn implements IBlueConn{

	// Global variables
	public static UUID[] uuids;
	Activity context;
	public static BluetoothAdapter bluetoothAdapter;
	public static Runnables run= null;
	int currentPolicies =0 ;
	static HashMap<String,Boolean> mapOfDevice= new HashMap<String,Boolean>();
	static HashMap<String,Boolean> mapActive= new HashMap<String,Boolean>();
	public static HashMap<String,String> mapOfRssi= new HashMap<String,String>();

	static BlueConn blue;
	static BlueConnHandler handler;
	
	//Tempo di refresh
	int refreshTime=12;
	
	static List<BlueConnectionDevice> listNeighbor=new ArrayList<BlueConnectionDevice>();;
	static List<BluetoothDevice> listToConnect;

	//Need for repetition of discovery
	Handler mHandler;
	int mInterval=Integer.MAX_VALUE;

	//
	//---------------------------------------------------------
	// Initializing
	/**
	 * Constructor
	 * @param context
	 */
	public BlueConn(Activity context,BlueConnHandler handlerB,UUID[] uuids){
		blue=this;
		BlueConn.uuids= uuids;
		this.context=context;
		handler= handlerB;
		mHandler = new Handler();

		listToConnect= new ArrayList<BluetoothDevice>();
		run=new Runnables(blue, new AHandler());
		initBluetooth();
	}


	public static BlueConn getInstance(){
		return blue;
	}

	/**
	 *  Initialize bluetooth.
	 *  automatically exit if the device isn't supported
	 */
	@Override
	public void initBluetooth() {
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (bluetoothAdapter == null) {
			if(D){ 
				Toast.makeText(context, "Bluetooth isn't supported.", Toast.LENGTH_LONG).show();
				Log.d("DEBUG", "Bluetooth isn't supported.");
			}
			context.finish();
		}
		checkEnabled();
	}

	/**
	 * if the Bluetooth is enabled or not
	 * 
	 * Provide on your activity an onActivityResult(int requestCode, int resultCode, Intent data) in which you handle case
	 * where request code is BlueConn.REQUEST_ENABLE_BLUETOOTH
	 */
	private void checkEnabled() {
		if (!bluetoothAdapter.isEnabled()) {
			Intent enable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			context.startActivityForResult(enable, REQUEST_ENABLE_BLUETOOTH);
			Toast.makeText(context, "Bluetooth is enabled.", Toast.LENGTH_LONG).show();
			if(D) Log.d("DEBUG", "Bluetooth is enabled.");
		} 		
	}


	/**
	 * It is needed when the device is no more discoverable
	 * 
	 */
	public void ensureDiscoverable() {
		if(D) Log.d(TAG, "ensure discoverable");
		if (bluetoothAdapter.getScanMode() !=
				BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
			context.startActivity(discoverableIntent);
		}
	}

	public void clear(){
		bluetoothAdapter = null;
		blue=null;
		run = null;
		handler=null;
		listNeighbor.clear();
		listToConnect.clear();
		mapOfDevice.clear();
		mapOfRssi.clear();
		unregisterBlueReceiver();
	}


	/**
	 * Start the threads
	 */
	@Override
	public void start(int mode){
		switch(mode){
		case STARTMODE_P2P:
			startAcceptingConnectionWithOtherDevices();
			startConnectionWithOtherDevices();
			break;
		case STARTMODE_CLIENT:
			startConnectionWithOtherDevices();
			break;
		case STARTMODE_SERVER:
			startAcceptingConnectionWithOtherDevices();
			break;			
		}
	}


	//--------------------------------------------------------
	// Connection request
	//TODO: create a function that request to connect to a device with same UUID.

	private void startConnectionWithOtherDevices(){
		if(run == null){

		} else {
			handler.obtainMessage(BlueConnHandler.MSG_CONNECTINGTHREAD, "Activated").sendToTarget();
			registerBlueReceiver();
			doDiscovery();
		}
	}

	private void registerBlueReceiver() {
		// Register for broadcasts when a device is discovered
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothDevice.ACTION_UUID);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		context.registerReceiver(mReceiver, filter);
	}

	public void unregisterBlueReceiver(){
		context.unregisterReceiver(mReceiver);
		//context.unregisterReceiver(mRssiUpdateReceiver);
	}


	/**
	 * Start device discover with the BluetoothAdapter
	 */
	private void doDiscovery() {
		if (D) Log.d(TAG, "doDiscovery()");
		handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "DODISCOVERY").sendToTarget();
		//mNewDevicesArrayAdapter.clear();
		// Indicate scanning in the title
		// If we're already discovering, stop it
		if (bluetoothAdapter.isDiscovering()) {
			bluetoothAdapter.cancelDiscovery();
		}
		// Request discover from BluetoothAdapter
		bluetoothAdapter.startDiscovery();
		startRepeatingTask(refreshTime);

	}


	private final BroadcastReceiver mReceiver= new BroadcastReceiver() {
		public List<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();

		@Override
		public void onReceive(Context context, Intent data) {

			String action = data.getAction();
			if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
				Log.d("DEBUG", "Scanning has finished.");
				handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "RECEIVER : Scanning has finished").sendToTarget();

				for(int i= 0; i<devices.size();i++){
					if(!devices.get(i).fetchUuidsWithSdp()){
						handler.obtainMessage(BlueConnHandler.MSG_ERROR, "SDP Error").sendToTarget();

					}
				}
				
				for(int i = 0 ; i < listToConnect.size(); i++){
					run.executeConnect(listToConnect.get(i), bluetoothAdapter);
				}

				mapActive.clear();
				//////////////////////////////////////////////////////
				handler.obtainMessage(BlueConnHandler.MSG_DISCOVERYENDED).sendToTarget();

			} else if (action.equals(BluetoothDevice.ACTION_FOUND)) {
				handler.obtainMessage(BlueConnHandler.MSG_DEBUG, "RECEIVER : Action found").sendToTarget();

				BluetoothDevice device = (BluetoothDevice) data.getExtras().get(BluetoothDevice.EXTRA_DEVICE);
				String com = device.getName()+"/"+device.getAddress();
				short rssi = data.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);
				mapOfRssi.put(com, rssi+"");
				for(int i=0; i< listNeighbor.size(); i++){
					String nameAdd=listNeighbor.get(i).getDevice().getName()+"/"+listNeighbor.get(i).getDevice().getAddress();
					if(nameAdd.equals(com)){
						BlueConnectionDevice bcd= new BlueConnectionDevice();
						boolean connected = false;
						for(int j = 0 ; j < Runnables.manageThreads.size(); j++ ){
							BluetoothDevice btD=Runnables.manageThreads.get(j).getBluetoothDeviceConnected();
							String namBtD= btD.getName()+"/"+btD.getAddress();
							if(namBtD.equals(nameAdd)){
								connected = true;
							}
						}

						if(connected){
							bcd.setMode(BlueConnectionDevice.MODE_CONNECTED);
						} else {
							bcd.setMode(BlueConnectionDevice.MODE_ACTIVE);
						}
						bcd.setDevice(device);
						bcd.setRssi(""+rssi);

						listNeighbor.set(i,bcd);

						Comparator<BlueConnectionDevice> c= new Comparator<BlueConnectionDevice>() {

							@Override
							public int compare(BlueConnectionDevice lhs,BlueConnectionDevice rhs) {
								// TODO Auto-generated method stub
								int rssi1= Integer.parseInt(lhs.getRssi());
								int rssi2=Integer.parseInt(rhs.getRssi());

								return rssi2-rssi1;
							}

						};

						Collections.sort(listNeighbor,c);
						handler.obtainMessage(BlueConnHandler.MSG_RSSIREFRESH).sendToTarget();
					}
				}
				devices.add(device);
				if(!bluetoothAdapter.isDiscovering()){
					bluetoothAdapter.startDiscovery();
				}
			} else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {
				handler.obtainMessage(BlueConnHandler.MSG_ONDISCOVERY).sendToTarget();

				devices.clear();

				Log.d("DEBUG", "Scanning has started.");
			} else if(action.equals(BluetoothDevice.ACTION_UUID)){

				BluetoothDevice device = data.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				Parcelable[] uuidExtra = data.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
				if(uuidExtra == null){
					handler.obtainMessage(BlueConnHandler.MSG_ERROR, "UUID EXTRA IS NULL").sendToTarget();
				} else {
					boolean present= false;
					// scorre la lista dei fetch uuids
					for(int i=0; i<uuidExtra.length;i++){
						// scorre la lista dei miei uuids
						for(int k=0;k<uuids.length;k++){

							if(uuidExtra[i].toString().equals(uuids[k].toString())){

								String conn = device.getName()+"/"+device.getAddress();
								for(int j=0; j<listNeighbor.size();j++){
									String nameAdd=listNeighbor.get(j).getDevice().getName()+"/"+listNeighbor.get(j).getDevice().getAddress();
									if(nameAdd.equals(conn)){
										present = true;
									}
								}
								if(!present && !mapOfDevice.containsKey(conn)){
									mapOfDevice.put(device.getName()+"/"+device.getAddress(),true);

									handler.obtainMessage(BlueConnHandler.MSG_DEBUG, device.getName()+"/"+device.getAddress()).sendToTarget();
									k=uuids.length;
									i=uuidExtra.length;

									BlueConnectionDevice bcd= new BlueConnectionDevice();
									bcd.setMode(BlueConnectionDevice.MODE_ACTIVE);
									bcd.setDevice(device);
									bcd.setRssi("0");

									listNeighbor.add(bcd);
									handler.obtainMessage(BlueConnHandler.MSG_NEWNEIGHBOR).sendToTarget();
									///////
								}
							} 

						}
					}
				}
			}
		}
	};

	public OnItemClickListener deviceListener = new OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			BluetoothDevice device = listNeighbor.get(position).getDevice();
			BlueConnectionDevice bcd= new BlueConnectionDevice();
			bcd.setDevice(device);
			bcd.setMode(BlueConnectionDevice.MODE_CONNECTING);
			bcd.setRssi( listNeighbor.get(position).getRssi());
			listNeighbor.set(position,bcd);
			handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTING).sendToTarget();

			// uso un arraylist per aggiungere i dispositivi da connettere
			// quando è finita la discovery mi connetto per rendere il processo di connessione molto più veloce
			listToConnect.add(device);
			//run.executeConnect(device, bluetoothAdapter);
		}

	};



	/**
	 * 
	 * @return  retrieve the devices' list that a user can contact
	 */
	public List<BlueConnectionDevice> whoAreMyNeighbor(){
		return listNeighbor;
	}
	//---------------------------------------------------------
	// Accepting request
	//TODO: create a function that accept request of connection.

	/**
	 * 
	 */
	protected void startAcceptingConnectionWithOtherDevices(){
		if(run == null){

		} else {
			for(int i=0; i<uuids.length;i++){
				run.executeAccept(bluetoothAdapter, i);
			}
			handler.obtainMessage(BlueConnHandler.MSG_ACCEPTINGTHREAD, "Activated").sendToTarget();
		}
	}

	//---------------------------------------------------------
	// Handlers
	/**
	 * 
	 */
	Runnable mStatusChecker = new Runnable() {
		@Override 
		public void run() {
			if(!bluetoothAdapter.isDiscovering()){
				bluetoothAdapter.startDiscovery();
			}
			mHandler.postDelayed(mStatusChecker, mInterval);
		}
	};

	
	void startRepeatingTask(int mInterval) {
		this.mInterval= mInterval*1000;
		mStatusChecker.run(); 
	}

	void stopRepeatingTask() {
		mHandler.removeCallbacks(mStatusChecker);
	}

	/**
	 * 
	 * Define your own Handler extending AHandler
	 * It is needed for communication between ManageThreads and Activity
	 * 
	 * @author santonastasi
	 */
	public static class AHandler extends Handler {

		public AHandler() {}

		@Override
		public void handleMessage(Message msg){
			switch (msg.what) {
			case BlueConnHandler.MSG_WRITE:	
				handler.obtainMessage(BlueConnHandler.MSG_WRITE, msg.arg1, msg.arg2, msg.obj).sendToTarget();
				break;
			case BlueConnHandler.MSG_READ:		
				Log.d(TAG, "Message read");
				handler.obtainMessage(BlueConnHandler.MSG_READ,  msg.arg1, msg.arg2, msg.obj).sendToTarget();
				break;


			case BlueConnHandler.MSG_GOODBYENEIGHBOR:
				String com= (String) msg.obj;
				for(int i=0; i<listNeighbor.size();i++){
					String nameAdd=listNeighbor.get(i).getDevice().getName()+"/"+listNeighbor.get(i).getDevice().getAddress();
					if(nameAdd.equals(com)){
						BlueConnectionDevice bcdf= new BlueConnectionDevice();
						bcdf.setMode(BlueConnectionDevice.MODE_ACTIVE);
						bcdf.setDevice(listNeighbor.get(i).getDevice());
						bcdf.setRssi("0");
						listNeighbor.set(i,bcdf);
						if (mapOfDevice.containsKey(com)){
							mapOfDevice.remove(com);
							mapOfRssi.remove(com);
						}
						handler.obtainMessage(BlueConnHandler.MSG_GOODBYENEIGHBOR,msg.obj).sendToTarget();
					}
				}
				blue.startAcceptingConnectionWithOtherDevices();


				break;

			case BlueConnHandler.MSG_DISCOVERYENDED:
				handler.obtainMessage(BlueConnHandler.MSG_DISCOVERYENDED,msg.obj).sendToTarget();
				break;


			case BlueConnHandler.MSG_STATECONNECTED:
				BlueConnectionDevice bcdg= null;
				String conn= (String) msg.obj;//
				boolean present = false;
				for(int i=0; i<listNeighbor.size();i++){
					String nameAdd=listNeighbor.get(i).getDevice().getName()+"/"+listNeighbor.get(i).getDevice().getAddress();
					if(nameAdd.equals(conn)){
						present = true;
						bcdg= new BlueConnectionDevice();
						bcdg.setMode("Connected");
						bcdg.setDevice(listNeighbor.get(i).getDevice());
						bcdg.setRssi(mapOfRssi.get(conn));
						mapOfDevice.put(conn, true);
						listNeighbor.set(i,bcdg);
						handler.obtainMessage(BlueConnHandler.MSG_REFRESH).sendToTarget();
						break;
					}
				}
				if(!present){
					bcdg= new BlueConnectionDevice();
					bcdg.setMode("Connected");
					Log.d("SIZEEE",""+ msg.arg1);
					bcdg.setDevice(Runnables.manageThreads.get(msg.arg1).getBluetoothDeviceConnected());
					bcdg.setRssi(mapOfRssi.get(conn));
					mapOfDevice.put(conn, true);
					listNeighbor.add(bcdg);
					handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTED).sendToTarget();
				}

			case BlueConnHandler.MSG_STATECONNECTING:
				handler.obtainMessage(BlueConnHandler.MSG_STATECONNECTED).sendToTarget();
				break;

			}
		}
	}

	/**
	 * 
	 * @param text contains the text of the message
	 * @param receiver contains the name and address formatted of the device you want to text
	 */
	public void write(String text, String receiver, int mode){
		switch(mode){
		case WRITE_SINGLEPEER:
			int numberOfSameThread=0;
			Log.d("2 - BlueConn", receiver + " " + text);
			boolean written = false;
			for(int i = 0 ; i< Runnables.manageThreads.size(); i++){
				String nameI=Runnables.manageThreads.get(i).getBluetoothDeviceConnected().getName()+"/"+Runnables.manageThreads.get(i).getBluetoothDeviceConnected().getAddress();
				if(nameI.equals(receiver)){

					Runnables.manageThreads.get(i).write(text.getBytes(), numberOfSameThread, receiver);
					numberOfSameThread++;

					written=true;
				}
			}
			if(!written){
				handler.obtainMessage(BlueConnHandler.MSG_ERROR, "The people you want to text is not connected").sendToTarget();
			}
			break;
		case WRITE_BROADCAST:
			if(bluetoothAdapter.isDiscovering()){
				bluetoothAdapter.cancelDiscovery();
			}
			stopRepeatingTask();
			List<BlueConnectionDevice> bcdlist=listNeighbor;
			for(int i = 0 ; i< bcdlist.size(); i++){
				boolean present= false;
				String nameNeighbor = bcdlist.get(i).getDevice().getName()+"/"+listNeighbor.get(i).getDevice().getAddress();

				for(int j = 0 ; j< Runnables.manageThreads.size(); j++){
					String nameManage=Runnables.manageThreads.get(j).getBluetoothDeviceConnected().getName()+"/"+Runnables.manageThreads.get(j).getBluetoothDeviceConnected().getAddress();
					if(nameNeighbor.equals(nameManage)){
						write(text, nameManage,0);
						present = true;
						Runnables.manageThreads.get(j).cancel();
						j= Runnables.manageThreads.size();
					}

				}

				if(!present){
					run.executeConnect2All(bcdlist.get(i).getDevice(), bluetoothAdapter,text);
				}

			}

			startRepeatingTask(refreshTime);
			break;

		}

	}


	//---------------------------------------------------------
}
