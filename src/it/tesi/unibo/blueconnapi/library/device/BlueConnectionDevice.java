package it.tesi.unibo.blueconnapi.library.device;

import it.tesi.unibo.blueconnapi.library.interfaces.IBlueConnectionDevice;
import android.bluetooth.BluetoothDevice;

public class BlueConnectionDevice implements IBlueConnectionDevice{

	BluetoothDevice device;
	String rssi;
	String mode;
	
	@Override
	public String getRssi() {
		return rssi;
	}
	
	@Override
	public void setRssi(String rssi) {
		this.rssi = rssi;
	}
	
	@Override
	public BluetoothDevice getDevice() {
		return device;
	}
	
	@Override
	public void setDevice(BluetoothDevice device) {
		this.device = device;
	}
	
	@Override
	public String getMode() {
		return mode;
	}
	
	@Override
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	
	
	
}
