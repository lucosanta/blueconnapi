# README #

This README is written to explain the usage of BlueConnAPI for Bluetooth connection with Android

* This library allows you any type of Bluetooth communication with Android.
* Version 1.0

* First of all you need to install ADT or Android Plugins for Eclipse
* Then import the project.
* Then to start using this library, create a new instance of BlueConnAPI. (For example of usage, please download BlueConnUsage)
* Then make it starts.
* For retrieving any type of message you have to create a class that extends BlueConnHandler and use for the message type the code you can find in IBlueConnHandler in interfaces package.

* For any issue contact me at luca.santonastasi@gmail.com